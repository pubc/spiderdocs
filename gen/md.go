package gen

import (
	"crypto/tls"
	"fmt"
	md "github.com/JohannesKaufmann/html-to-markdown"
	"github.com/PuerkitoBio/goquery"
	"io"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"spiderdocs/util"
	"strings"
	"time"
)

// ToMD Print to markdown
func ToMD(url string) {
	tr := &http.Transport{
		TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		ResponseHeaderTimeout: 20 * time.Second,
		ExpectContinueTimeout: 20 * time.Second,
	}
	client := &http.Client{
		Transport:     tr,
		CheckRedirect: RedirectHandler,
	}

	resp, err := client.Get(url)
	if err != nil {
		return
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)

	doc, err := goquery.NewDocumentFromReader(resp.Body)
	if err != nil {
		fmt.Printf("generate new document faild: %s\n", err)
	}

	// Get html title
	title := doc.Find("h1").Text()

	newDoc := classRemove(doc)
	markdown := elementsFind(newDoc)

	// Match forum.gitlab.com url
	re := regexp.MustCompile("http(s)?://forum.gitlab.*")
	if re.MatchString(url) {
		markdown = forumClarify(markdown)
	}

	// Renew html title
	re = regexp.MustCompile("^# .*")
	if !re.MatchString(markdown) {
		markdown = fmt.Sprintf("# %s\n%s", util.NameTrim(title), markdown)
	}

	_ = os.Mkdir("markdowns", os.ModePerm)
	newTitle := fmt.Sprintf("markdowns/%s.md", util.Naming(title))
	util.SaveFile(newTitle, []byte(markdown))
}

// RedirectHandler Handling redirected URLs
func RedirectHandler(req *http.Request, via []*http.Request) error {
	// req - new request will be access
	// via old request

	req.URL, _ = url.Parse(req.URL.String() + "/")

	// You can choose to ignore the redirect and return a specific error to abort the redirect
	// return http.ErrUseLastResponse
	return nil
}

// elementsFind Find fixed elements and convert the matched content into Markdown syntax
func elementsFind(doc *goquery.Document) string {
	elements := []string{
		"article",
		"main",
		".content",
		"body",
	}

	converter := md.NewConverter("", true, nil)

	for _, ele := range elements {
		markdown := converter.Convert(doc.Find(ele))
		if markdown != "" {
			return markdown
		}
	}
	return ""
}

// classRemove Remove multiple elements from html css
func classRemove(doc *goquery.Document) *goquery.Document {
	classes := []string{
		".twitter-block",
		".customer-service-container",
		".category-back-link",
		".wrapper related",
		".topic-meta-data",
		"header",
		"nav",
		"aside",
		"footer",
		"script",
		"img",
		"slp-blog",
	}

	for _, class := range classes {
		doc.Find(class).Remove()
	}
	return doc
}

func forumClarify(str string) string {
	lines := strings.Split(str, "\n")

	var newStr string
	var newLines []string
	brokenHeadLevel := regexp.MustCompile("#[0-9]+")
	//nameRe := regexp.MustCompile(`^.([a-zA-Z.]+)(.{1,2})?(https://.*)?$`)
	nameRe := regexp.MustCompile(`^(\[.*]\(.*\))$`)
	dateRe := regexp.MustCompile(`^.*[0-9]{1,2},.[0-9]{4},.[0-9]{1,2}:[0-9]{2}[a-z]{2}`)
	actionRe := regexp.MustCompile(`^[0-9]+(.[a-zA-Z]+)?`)
	blankLineRe := regexp.MustCompile(`\n\s*\n(\s*\n)+`)
	brokenLinkRe := regexp.MustCompile(`^(\[.*]\(((\.)+)?(/)([a-z0-9.]+/)+.*\))$`)

	for _, line := range lines {
		if nameRe.MatchString(line) || dateRe.MatchString(line) || actionRe.MatchString(line) || brokenHeadLevel.MatchString(line) || brokenLinkRe.MatchString(line) {
			continue
		}

		newLines = append(newLines, line)
	}

	newStr = strings.Join(newLines, "\n")
	if blankLineRe.MatchString(newStr) {
		newStr = blankLineRe.ReplaceAllString(newStr, "\n\n")
	}
	return newStr
}
