package gen

import (
	"context"
	"fmt"
	"github.com/gocolly/colly"
	"log"
	"regexp"
	"time"
)

type PrintFunc func(string)

type PrintAccess struct {
	TargetURL  *string   // top url will be access
	PrintFunc  PrintFunc // function to gen text or pdf
	MaxDepth   *int      // access depth
	IgnoreExpr *string   // return urls not matched
	MatchExpr  *string   // return urls matched
	WaitTime   *int
}

func BreakdownAccess(pa PrintAccess) {

	// Generate new Collector
	c := colly.NewCollector(
		colly.MaxDepth(*pa.MaxDepth),
	)

	// Callback function executed before accessing the website
	c.OnRequest(func(r *colly.Request) {
		re1, _ := regexp.Compile(*pa.IgnoreExpr)
		if re1.MatchString(r.URL.String()) {
			return
		}
		re2, _ := regexp.Compile(*pa.MatchExpr)
		if !re2.MatchString(r.URL.String()) && *pa.MatchExpr != "nil" {
			return
		}

		log.Println("Visiting", r.URL)
		funcWithTimeout(pa.PrintFunc, r.URL.String())
		time.Sleep(time.Duration(*pa.WaitTime) * time.Second)
	})

	// Callback function executed before parsing the web page
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		if !isURLValid(link) {
			domain := e.Request.URL.Scheme + "://" + e.Request.URL.Host
			link = domain + link
		}

		_ = e.Request.Visit(link)
	})

	// Start traversing
	err := c.Visit(*pa.TargetURL)
	if err != nil {
		log.Fatalf("visit url %s failed: %s\n", *pa.TargetURL, err)
	}
}

func funcWithTimeout(pf PrintFunc, url string) {
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	pf(url)

	for {
		select {
		default:
			return
		case <-ctx.Done():
			fmt.Println(ctx.Err().Error())
			return
		}
	}
}

func isURLValid(url string) bool {
	regex := regexp.MustCompile(`^(https?|ftp)://[^\s/$.?#].[^\s]*$`)
	return regex.MatchString(url)
}
