package gen

import (
	"context"
	"crypto/tls"
	"fmt"
	"github.com/PuerkitoBio/goquery"
	wk "github.com/SebastiaanKlippert/go-wkhtmltopdf"
	"github.com/chromedp/cdproto/page"
	"github.com/chromedp/chromedp"
	"golang.org/x/net/html"
	"io"
	"log"
	"net/http"
	"os"
	"spiderdocs/util"
	"time"
)

/*
Different packages gen pdf with different effects.
Two are mainly used here:
- wkhtmltopdf
- chromedp
*/

// ToPdfW Using go-wkhtmltopdf to gen pdfs
func ToPdfW(url string) {
	// Generate new context
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	// Create new PDF generator
	pdfg, err := wk.NewPDFGenerator()
	if err != nil {
		log.Fatalf("could not init a new pdfGenerator: %s\n", err)
	}

	// Set global options
	pdfg.NoCollate.Set(false)
	pdfg.PageSize.Set(wk.PageSizeA4)

	// Add one page from a URL
	pdfg.AddPage(wk.NewPage(url))

	// Create PDF document in internal buffer
	err = pdfg.Create()
	if err != nil {
		log.Fatalf("could not create PDF: %s\n", err)
	}

	// Write buffer contents to file on disk
	_ = os.Mkdir("pdfs", os.ModePerm)
	newFile := fmt.Sprintf("pdfs/%s.pdf", util.Naming(getTitle(url)))

	err = pdfg.WriteFile(newFile)
	if err != nil {
		log.Fatalf("could not save file %s: %s\n", newFile, err)
	} else {
		log.Printf("page saved to %s\n", newFile)
	}

	select {
	case <-ctx.Done():
		log.Printf("context deadline exceeded: %s\n", url)
	default:
		return
	}
}

// ToPdfC Using chromedp to gen pdfs
func ToPdfC(url string) {
	// Generate new context
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()

	ctx, cancel = chromedp.NewContext(ctx)
	defer cancel()

	// Create PDF document
	var buf []byte
	if err := chromedp.Run(ctx, printToPDF(url, &buf)); err != nil {
		log.Fatalf("print to pdf failed: %s\n", err)
	}

	_ = os.Mkdir("pdfs", os.ModePerm)
	newFile := fmt.Sprintf("pdfs/%s.pdf", util.Naming(getTitle(url)))

	util.SaveFile(newFile, buf)
}

// printToPDF Generate chromedp tasks list
func printToPDF(url string, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(url),
		chromedp.ActionFunc(func(ctx context.Context) error {
			printOpts := page.PrintToPDFParams{
				Landscape:       true,
				PrintBackground: true,
			}
			buf, _, err := printOpts.Do(ctx)
			if err != nil {
				return err
			}
			*res = buf
			return nil
		}),
	}
}

// getTitle Get html page h1 title
func getTitle(url string) string {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{
		Transport:     tr,
		CheckRedirect: RedirectHandler,
		Timeout:       20 * time.Second,
	}

	resp, err := client.Get(url)
	if err != nil {
		return ""
	}
	defer func(Body io.ReadCloser) {
		_ = Body.Close()
	}(resp.Body)

	// Convert "reader" to object "node" for multiple reads
	htmlNode, err := html.Parse(resp.Body)
	if err != nil {
		fmt.Printf("generate html.Node failed from reader: %s\n", err)
	}

	// Get title
	body := goquery.NewDocumentFromNode(htmlNode)
	if err != nil {
		fmt.Printf("read html.Body from html.Node failed: %s\n", err)
	}

	title := body.Find("h1").Text()

	return title
}
