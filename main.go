package main

import (
	"flag"
	"spiderdocs/gen"
)

func main() {
	url := flag.String("u", "https://www.example.com", "Top URL")
	outFormat := flag.String("f", "md", "Generate to md or pdf")
	accessDepth := flag.Int("d", 2, "Access depth")
	pdfEngine := flag.String("e", "chromedp", "Pdf generate by chromedp or wkhtmltopdf")
	ignoreUrls := flag.String("r", "nil", "Ignore urls by regular expression")
	matchUrls := flag.String("m", "nil", "Match urls by regular expression")
	waitInterval := flag.Int("i", 1, "Wait interval(second)")
	flag.Parse()

	var f gen.PrintFunc
	if *outFormat == "md" {
		f = gen.ToMD
	} else if *outFormat == "pdf" {
		if *pdfEngine == "wkhtmltopdf" {
			f = gen.ToPdfW
		} else if *pdfEngine == "chromedp" {
			f = gen.ToPdfC
		}
	}

	pa := gen.PrintAccess{
		TargetURL:  url,
		PrintFunc:  f,
		MaxDepth:   accessDepth,
		IgnoreExpr: ignoreUrls,
		MatchExpr:  matchUrls,
		WaitTime:   waitInterval,
	}
	gen.BreakdownAccess(pa)
}
