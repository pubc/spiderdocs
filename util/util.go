package util

import (
	"fmt"
	"github.com/google/uuid"
	"regexp"
	"strings"
	"unicode/utf8"
)

func Naming(name string) string {

	if name == "" {
		return fmt.Sprintf("NONAME_%d", uuid.New().ID())
	}

	re1, _ := regexp.Compile("[\\p{P}|\\p{Z}\\p{S}\\p{C}\\p{M}]")
	reName1 := re1.ReplaceAllString(name, "_")

	re2, _ := regexp.Compile("_{2,}")
	reName2 := re2.ReplaceAllString(reName1, "_")

	_, total := CountWords(reName2)
	if total >= 30 {
		reName2 = reName2[:31]
		for !utf8.ValidString(reName2) {
			reName2 = reName2[:len(reName2)-1]
		}
	}

	re3 := regexp.MustCompile(`^_|_$`)
	newTitle := re3.ReplaceAllString(reName2, "")
	return strings.ToLower(newTitle)
}

func CountWords(text string) (int64, int64) {
	// Count the number of Chinese characters
	hanRe := regexp.MustCompile("[\u4e00-\u9fa5]")
	hanRes := hanRe.FindAllString(text, -1)
	newHanText := strings.Join(hanRes, "")

	// Count the number of Chinese characters
	hanCharRe := regexp.MustCompile(`[^\x00-\xff]+`)
	hanCharRes := hanCharRe.FindAllString(text, -1)
	newHanCharText := strings.Join(hanCharRes, "")

	// Count the number of English words
	engRe := regexp.MustCompile("[a-zA-Z]+")
	engRes := engRe.FindAllString(text, -1)
	newEngText := strings.Join(engRes, " ")
	newEngTextSlice := strings.Fields(newEngText)

	// Count the number of Chinese characters
	engCharRe := regexp.MustCompile("[a-zA-Z]+|[[:punct:]]")
	engCharRes := engCharRe.FindAllString(text, -1)
	newEngCharText := strings.Join(engCharRes, " ")
	newEngCharTextSlice := strings.Fields(newEngCharText)

	// Number of Chinese characters and words
	pureWordsCnt := int64(utf8.RuneCountInString(newHanText)) + int64(len(newEngTextSlice))

	// All character count
	allWordsCnt := int64(utf8.RuneCountInString(newHanCharText)) + int64(len(newEngCharTextSlice))

	return pureWordsCnt, allWordsCnt
}

func NameTrim(str string) string {
	// Remove links within brackets
	re := regexp.MustCompile(`\([^)]+\)|\[|]|\\|\n`)
	newStr := re.ReplaceAllString(str, "")
	return newStr
}
