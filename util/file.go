package util

import (
	"bufio"
	"fmt"
	"log"
	"os"
)

func SaveFile(name string, content []byte) {
	file, err := os.OpenFile(name, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0660)
	if err != nil {
		fmt.Printf("open file failed: %s\n", err)
	}

	defer file.Close()

	writer := bufio.NewWriterSize(file, 2048)
	_, err = writer.Write(content)
	if err != nil {
		fmt.Printf("write file failed: %s\n", err)
	}

	err = writer.Flush()

	if err != nil {
		log.Printf("could not save file %s: %s\n", name, err)
	} else {
		log.Printf("file saved to %s.\n", name)
	}
}
