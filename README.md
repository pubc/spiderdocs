# SpiderDocs

## Description

This is a tool used to crawl specified pages as offline documents.

## Usage 

```shell
$ ./spiderdocs --help 

Usage of ./spiderdocs:
  -d int
        Access depth (default 2)
  -e string
        Pdf generate by chromedp or wkhtmltopdf (default "chromedp")
  -f string
        Generate to md or pdf (default "md")
  -i int
        Wait interval(second) (default 1)
  -m string
        Match urls by regular expression (default "nil")
  -r string
        Ignore urls by regular expression (default "nil")
  -u string
        Top URL (default "https://www.example.com")

```

### Examples

- Print to Markdown

```shell
$ ./spiderdocs -u "https://baike.baidu.com/item/OpenCSG" -f md
```

- Print to PDF

```shell
$ ./spiderdocs -u "https://baike.baidu.com/item/OpenCSG" -f pdf
```

- Specify crawl depth

```shell
$ ./spiderdocs -u "https://baike.baidu.com/item/OpenCSG" -f pdf -m 4 
```

- Filter url
```shell
# Only crawler page with url matched
$ ./spiderdocs -u "https://baike.baidu.com/item/OpenCSG" -f pdf -m "https://.*/item/.*"

# Only crawler page with url not matched
$ ./spiderdocs -u "https://baike.baidu.com/item/OpenCSG" -f pdf -r "https://.*/item/.*"
```
